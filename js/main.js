'use strict'

/*
Теорія

1. Описати своїми словами навіщо потрібні функції у програмуванні.
У функцію можна записати певну послідовність дій над параметрами, що у неї передаються (аргументами),
після чого викликати її будь-яку кількість разів з різними аргументами без необхідності
повторювати код, що виконується, для кожного набору даних.

2. Описати своїми словами, навіщо у функцію передавати аргумент.
Аргументи для функції є змінними, що обробляються.
Аргументи потрібні у разі, якщо результат обробки залежить від вхідних даних.

3. Що таке оператор return та як він працює всередині функції?
Оператор return - повертає вказане значення - результат виконання функції і завершує роботу функції.

*/

console.log('Task 1')

/*
Checkup functions
*/

function numCheckup(num) {
    if (isNaN(+num) || +num == null) {
        while (isNaN(+num) || +num == null) {
            num = prompt('Enter a number!', num);
        }
    }

    return(num);
}

function actCheckup(act) {


    if (act !== '*' && act !== '/' && act !== '+' && act !== '-') {
        while (act !== '*' && act !== '/' && act !== '+' && act !== '-') {
            act = prompt('Enter a sign!', act);
        }
    }

    return(act);
}

/*
Get values
*/


let a = prompt('Enter a number 1');
let num1 = numCheckup(+a);
let b = prompt('Enter a number 2');
let num2 = numCheckup(+b);
let c = prompt('Enter an action sign');
let act = actCheckup(c);

/*
Function calculate
*/

function calculate(num1, num2, act) {
    let result;
    switch (act) {
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
    }

    return(result);
}

/*
Output
*/

console.log(`Your parameters is: num1 = ${num1}, num2 = ${num2}, act = ${act}`);
console.log(`Your result is: ${calculate(num1, num2, act)}`);

